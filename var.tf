variable "region" {
    type        = string
    description = "aws region type"
    default     = "us-east-2"
}
variable "counts" {
    description = "number of instance you want to provision"
    default     = 1
}
variable "devopskeys" {
    type  = string
    default = "Mysever"
}
variable "tag" {
    type = string
    default = "webserver"
}
variable "monitoring" {
    default = true
}
variable "instance_type" {
    default = "t2.micro"
}
variable "instanceTenancy" {
    default = "default"
}
variable "availability_zone" {
    default = "us-east-2a"
}
variable "cidr_block" {
    default = "10.0.1.0/24" 
}
variable "vpc_cidrblock" {
    default = "10.0.0.0/16"
}