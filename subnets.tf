resource "aws_subnet" "public_1" {
  # The VPC ID.
  vpc_id = aws_vpc.my_vpc.id
  # The CIDR block for the subnet.
  cidr_block = "10.0.1.0/24"
  # The AZ for the subnet.
  availability_zone = "us-east-2a"
  # Required for EKS. Instances launched into the subnet should be assigned a public IP address.
  map_public_ip_on_launch = true
  # A map of tags to assign to the resource.
  tags = {
    Name                        = "public-subnet-1"
  }
}

resource "aws_subnet" "public_2" {
  # The VPC ID.
  vpc_id = aws_vpc.my_vpc.id
  # The CIDR block for the subnet.
  cidr_block = "10.0.3.0/24"
  # The AZ for the subnet.
  availability_zone = "us-east-2b"
  # Required for EKS. Instances launched into the subnet should be assigned a public IP address.
  map_public_ip_on_launch = true
  # A map of tags to assign to the resource.
  tags = {
    Name                        = "public_subnet_2"
  }
}

resource "aws_subnet" "private_1" {
  # The VPC ID.
  vpc_id = aws_vpc.my_vpc.id
  # The CIDR block for the subnet.
  cidr_block = "10.0.4.0/24"
  # The AZ for the subnet.
  availability_zone = "us-east-2c"
  # A map of tags to assign to the resource.
  tags = {
    Name                              = "private_subnet_1"
  }
}

resource "aws_subnet" "private_2" {
  # The VPC ID.
  vpc_id = aws_vpc.my_vpc.id
  # The CIDR block for the subnet.
  cidr_block = "10.0.5.0/24"
  # The AZ for the subnet.
  availability_zone = "us-east-2a"
  # A map of tags to assign to the resource.
  tags = {
    Name                              = "private-subnet-2"
  } 
}