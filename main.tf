resource "aws_instance" "webserver" {
  count         = var.counts
  key_name      = var.devopskeys
  monitoring    = var.monitoring
  ami           = "ami-0a91cd140a1fc148a"
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.webserver.id]
  subnet_id = aws_subnet.my_subnet.id


  tags = {
    Name = var.tag
  }


ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 30
 }
}

